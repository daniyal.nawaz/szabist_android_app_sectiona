package com.example.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.app1.adapters.ContactAdapter
import com.example.app1.common.showToast
import com.example.app1.models.Contact
import com.example.app1.models.DataHolder
import kotlinx.android.synthetic.main.activity_dashboard.*
import java.util.concurrent.CopyOnWriteArraySet

class DashboardActivity : AppCompatActivity() {
    private var isAllowExit = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        val intent = intent

        //val username = intent.extras?.getString("username")
        /*val person = intent.extras?.getSerializable("personObject") as Person

        txUserName.text = "Welcome ${person.email}"*/

        /*val user = intent.extras?.getParcelable<User>("userObject")

        txUserName.text = "Welcome ${user!!.name}"*/

        val user = DataHolder.user
        txUserName.text = "Welcome ${user!!.email}"

        //rvList.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true)
        //rvList.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        rvList.layoutManager = GridLayoutManager(this,2)
        val datasource = getDatSource()
        val contactAdapter = ContactAdapter(datasource)

        rvList.adapter = contactAdapter

    }

    private fun getDatSource():ArrayList<Contact>{
        val list = ArrayList<Contact>()
        list.add(Contact(1,"Ahmed Ali","ahmed@gmail.com","+92-344-5454565",R.mipmap.ic_launcher_round))
        list.add(Contact(2,"Jazib Ali","jazib@gmail.com","+92-345-5454565",R.mipmap.ic_launcher_round))
        list.add(Contact(3,"Shahid Ali","shahid@gmail.com","+92-346-5454565",R.mipmap.ic_launcher_round))
        list.add(Contact(4,"Muhammad Ali","ali@gmail.com","+92-399-5454565",R.mipmap.ic_launcher_round))
        list.add(Contact(5,"Zeeshan","xyz@gmail.com","+92-322-5454565",R.mipmap.ic_launcher_round))
        list.add(Contact(6,"Haider","abc@gmail.com","+92-321-5454565",R.mipmap.ic_launcher_round))
        list.add(Contact(7,"Qasim","qasim@gmail.com","+92-312-5454565",R.mipmap.ic_launcher_round))
        return list
    }

    override fun onBackPressed() {
        //super.onBackPressed()

        if(isAllowExit){
            super.onBackPressed()
        }

        showToast("press back again to exit")
        isAllowExit=true

        Handler().postDelayed({isAllowExit=false},2000)

    }

}