package com.example.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.app1.common.showToast
import com.example.app1.database.AppDatabase
import com.example.app1.datasource.LocalStorageProvider
import com.example.app1.entities.Product
import com.example.app1.models.DataHolder
import com.example.app1.models.User
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var localStorageProvider:LocalStorageProvider
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        localStorageProvider = LocalStorageProvider(this)
        /*val txMess = findViewById<TextView>(R.id.txMessage)
        txMess.text = "Welcome Android"*/

        //findViewById<TextView>(R.id.txMessage).text = "Welcome Android"

        //txMessage.text = "Welcome Android"

        //Dummy Credentials
        //edEmail.setText("abc.ali@gmail.com")
        //edPassword.setText("123456")

        btnSignIn.setOnClickListener {
            onSignClicked()
        }

        //val email = localStorageProvider.getData("user_email")
        //val password = localStorageProvider.getData("user_password")

        val userStr = localStorageProvider.getData("userStr")
        if(userStr.isNotEmpty()){
            val gson = Gson();
            val user  =  gson.fromJson(userStr, User::class.java)
            DataHolder.user = user
            goToDashboard(user.email)
        }

        //if(email.isNotEmpty() && password.isNotEmpty()){
            //goToDashboard(email)
        //}
        //btnSignIn.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.btnSignIn -> onSignClicked()//showToast("onSignInClicked")
        }
        /*if(p0!!.id == R.id.btnSignIn){
            showToast("onSignInClicked")
        }*/
    }

    private fun onSignClicked(){
        val emailRegex = Regex("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})\$")
        val passwordRegex = Regex("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%]).{6,15})")

        val email = edEmail.text.toString().trim()
        val password = edPassword.text.toString().trim()

        if(email.isEmpty()){
            showToast("please enter email address")
            return
        }

        /*if(!emailRegex.matches(email)){
            showToast("please enter valid email address")
            return
        }*/

        if(password.isEmpty()){
            showToast("please enter valid password")
            return
        }

       /* if(!passwordRegex.matches(password)){
            showToast("please enter password")
            return
        }*/

        /*val intent = Intent(this, DashboardActivity::class.java)
        intent.putExtra("username", username)
        intent.putExtra("password", password)
        startActivity(intent)*/

        /*val person = Person(1,"Haider Saba",email,"+92356299993")
        Intent(this, DashboardActivity::class.java).apply {
            putExtra("personObject", person)
            startActivity(this)
        }*/


        /*val user = User(1,"Haider Saba",email,"+92356299993")
        Intent(this, DashboardActivity::class.java).apply {
            putExtra("userObject", user)
            startActivity(this)
        }*/

        //localStorageProvider.saveData("user_email", email)
        //localStorageProvider.saveData("user_password", password)

        /*val user = User(1,"Haider Saba",email,"+92356299993")
        DataHolder.user = user*/
        val user = User(1,"Haider Saba",email,"+92356299993")
        val gson = Gson();

        val userStr = gson.toJson(user)
        DataHolder.user = user
        localStorageProvider.saveData("userStr", userStr)

        goToDashboard(email)


        /*intent.putExtra("username", username)
        intent.putExtra("password", password)
        startActivity(intent)*/
    }

    private fun goToDashboard(email:String){

        Intent(this, DashboardActivity::class.java).apply {
            startActivity(this)
            finish()
        }
    }

    /*fun onSignInClicked(view: View) {
        //showToast("onSignInClicked")
    }*/
}