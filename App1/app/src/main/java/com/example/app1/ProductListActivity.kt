package com.example.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.app1.adapters.ProductAdapter
import com.example.app1.common.showToast
import com.example.app1.database.AppDatabase
import com.example.app1.entities.Product
import kotlinx.android.synthetic.main.activity_product_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class ProductListActivity : AppCompatActivity(), CoroutineScope {
    private var job: Job = Job()
    private val context = this
    private var adapter:ProductAdapter? =null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)

        rvProductList.layoutManager = LinearLayoutManager(this)

        getAllProduct(1)
    }

    fun onAddProductClicked(view: View) {
        val name = edProductName.text.toString().trim()
        val price = edProductPrice.text.toString().trim()
        val quantity = edProductQuantity.text.toString().trim()

        if(name.isNotEmpty() && price.isNotEmpty() && quantity.isNotEmpty()){
            val product = Product(quantity = quantity.toInt(), price = price,name = name)
            addProduct(product)
        }
    }

    private fun addProduct(product: Product){
        launch {
           val id =  AppDatabase.getDatabase(context).productDAO().insertOrUpdate(product)
           showToast("Product Id = ${id}")
            getAllProduct(2)
        }
    }

    private fun getAllProduct(operation:Int){
        launch {
            val productList =  AppDatabase.getDatabase(context).productDAO().getAllProducts()
            if(operation==1) renderList(productList)
            else refreshList(productList)
        }
    }

    private fun renderList(productList:List<Product>){
        adapter = ProductAdapter(productList)
        rvProductList.adapter = adapter
    }

    private fun refreshList(productList:List<Product>){
        adapter?.setItems(productList)
        adapter?.notifyDataSetChanged()
    }
}