package com.example.app1

import android.app.AlertDialog
import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.app1.R
import com.example.app1.common.showToast
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.text.SimpleDateFormat
import java.util.*

class SignUpActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private val cal = Calendar.getInstance()
    private val genderArr = arrayOf("Select Gender","Male","Female","Other")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        updateDOBOnView()
        bindGenderSpinner()

        val dateSelectionListener = DatePickerDialog.OnDateSetListener{ datePicker, year, month, day ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, month)
            cal.set(Calendar.DAY_OF_MONTH, day)
            updateDOBOnView()
        }

        icDOB.setOnClickListener {
            DatePickerDialog(this@SignUpActivity,
                dateSelectionListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

    private fun bindGenderSpinner(){
        //val adapter = ArrayAdapter.createFromResource(this,R.array.gender_array, android.R.layout.simple_spinner_item)

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, genderArr)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spGender.adapter = adapter

        spGender.onItemSelectedListener = this
    }

    private fun updateDOBOnView(){
        val dobFormat = "dd-MM-YYYY"
        val dobFormatter = SimpleDateFormat(dobFormat, Locale.US)
        txDOB.text = dobFormatter.format(cal.time)
    }

    fun onSignUpClicked(view: View) {
        val message = "RB 10 Above = ${rb10Above.isChecked}" +
                "\nRB 18 Above = ${rb18Above.isChecked}" +
                "\nRB 31 Above = ${rb31Above.isChecked}" +
                "\n\nTerms & Condition = ${chTermsConditions.isChecked}"

        showAlert("Selection", message)
    }

    private fun showAlert(title: String, message: String){
        AlertDialog.Builder(this).apply {
            setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok"){ dialog, i->
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel"){ dialog, i->
                    dialog.dismiss()
                }
        }.create().show()
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        //showToast(p0?.getItemAtPosition(p2).toString())
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }
}