package com.example.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.example.app1.common.showToast
import com.example.app1.models.User

class SplashActivity : AppCompatActivity() {
    //private var runnable: Runnable? = null
    private lateinit var runnable: Runnable
    //private var user:User? = null
    private val handler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        //val email = user?.email?:""
        /*val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)*/
        //runnable = null
        runnable = Runnable {
            val intent = Intent(this,ProductListActivity::class.java)
            startActivity(intent)
            finish()
        }

        //handler.postDelayed(runnable?: Runnable {  },3000)
        handler.postDelayed(runnable,3000)
    }

    override fun onDestroy() {
        handler.removeCallbacks(runnable)
        super.onDestroy()
        //Toast.makeText(this,"onDestroy()",Toast.LENGTH_SHORT).show()

        //showToast("onDestroy()")
    }
}