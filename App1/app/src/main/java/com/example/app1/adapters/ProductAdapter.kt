package com.example.app1.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.app1.R
import com.example.app1.entities.Product
import com.example.app1.viewholders.ProductViewHolder

class ProductAdapter(var productList:List<Product>):RecyclerView.Adapter<ProductViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product,parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = productList[position]
        holder.txName.text = product.name
        holder.txPrice.text = product.price
        holder.txQuantity.text = product.quantity.toString()
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    fun setItems(newProductList:List<Product>){
        productList = newProductList
    }
}