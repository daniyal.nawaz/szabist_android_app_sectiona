package com.example.app1.dao

import androidx.room.*
import com.example.app1.entities.Product

@Dao
interface ProductDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdate(product: Product):Long

    @Query("SELECT * FROM products")
    suspend fun getAllProducts():List<Product>

    @Query("SELECT * FROM products WHERE id = :id")
    suspend fun getProductById(id:Int):Product

    @Delete
    suspend fun delete(product: Product)
}