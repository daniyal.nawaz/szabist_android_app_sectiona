package com.example.app1.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.app1.dao.ProductDAO
import com.example.app1.entities.Product

@Database(entities = arrayOf(Product::class), version = 1, exportSchema = false)
abstract class AppDatabase:RoomDatabase() {
    abstract fun productDAO():ProductDAO

    companion object {
        private var DB_INSTANCE:AppDatabase?=null

        fun getDatabase(context: Context):AppDatabase{
            return DB_INSTANCE?: synchronized(this){
                val instance = Room.databaseBuilder(context, AppDatabase::class.java,"db_app_1").build()
                DB_INSTANCE = instance
                instance
            }
        }
    }
}