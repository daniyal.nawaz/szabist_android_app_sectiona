package com.example.app1.datasource

import android.content.Context

class LocalStorageProvider(context: Context) {
    private val sharedPref = context.getSharedPreferences("my_app_pref", Context.MODE_PRIVATE)

    fun getData(key:String):String{
        return sharedPref.getString(key,"")?:""
    }

    fun saveData(key:String, value:String){
        sharedPref.edit().apply {
            putString(key, value)
            apply()
        }
    }
}