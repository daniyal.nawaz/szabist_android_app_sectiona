package com.example.app1.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "products")
data class Product(
    @PrimaryKey(autoGenerate = true) val id:Int = 0,
    @ColumnInfo(name="product_name") val name:String,
    @ColumnInfo(name="product_price") val price:String,
    @ColumnInfo(name="product_quantity") val quantity:Int
)
