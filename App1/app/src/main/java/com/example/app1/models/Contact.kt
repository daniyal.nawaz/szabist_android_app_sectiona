package com.example.app1.models

data class Contact(
    val id:Int,
    val name:String,
    val email:String,
    val mobileNo:String,
    val image:Int
)
