package com.example.app1.models

import java.io.Serializable

data class Person(
    val id:Int,
    val name:String,
    val email:String,
    val mobileNo:String
):Serializable
