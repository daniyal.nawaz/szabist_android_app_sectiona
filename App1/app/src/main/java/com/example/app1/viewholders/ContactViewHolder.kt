package com.example.app1.viewholders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.app1.R

class ContactViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
    val imgUser = itemView.findViewById<ImageView>(R.id.imgUser)
    val txName = itemView.findViewById<TextView>(R.id.txName)
    val txEmail = itemView.findViewById<TextView>(R.id.txEmail)
    val txMobileNo = itemView.findViewById<TextView>(R.id.txMobileNo)
}