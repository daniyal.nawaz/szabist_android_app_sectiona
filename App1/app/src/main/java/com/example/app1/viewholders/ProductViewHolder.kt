package com.example.app1.viewholders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.app1.R

class ProductViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
    val txName = itemView.findViewById<TextView>(R.id.txName)
    val txPrice = itemView.findViewById<TextView>(R.id.txPrice)
    val txQuantity = itemView.findViewById<TextView>(R.id.txQuantity)
}